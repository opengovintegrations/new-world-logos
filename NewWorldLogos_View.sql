CREATE VIEW vw_logos_opengov_transactions_with_ap_detail_V2
AS
WITH cte AS
( SELECT OrganizationDelimiter,
        OrgSetandAccountDelimiter,
     AccountDelimiter
 FROM LogosDB.dbo.LogosSystemSettings
)

SELECT  jd.JournalDetailID AS transactionID,
       JD.GLDate,
	     GLA.GLAccountCodeScrunched,
	     fnG.GLAccountDelimiter,
		   ISNULL(GLA.Org1Code, '') AS Org1Code,
		   ISNULL(os.Org1Description,'') AS Org1Description,
		   ISNULL(GLA.Org2Code,'') AS Org2Code,
	     ISNULL(os.Org2Description,'') AS Org2Description,
		   ISNULL(GLA.Org3Code,'') AS Org3Code,
	     ISNULL(os.Org3Description,'') AS Org3Description,
	     ISNULL(GLA.Org4Code,'') AS Org4Code,
	     ISNULL(os.Org4Description,'') AS Org4Description,
		   ISNULL(GLA.Org5Code,'') AS Org5Code,
		   ISNULL(os.Org5Description,'') AS Org5Description,
	     ISNULL(GLA.Org6Code,'') AS Org6Code,
		   ISNULL(os.Org6Description,'') AS Org6Description,
		   ISNULL(FnA.AccountCode,'') AS AccountCode,
		   ISNULL(FnA.DetailCode,'') AS DetailCode,
		   ISNULL(FnA.SubDetailCode,'') AS SubDetailCode,
		   ISNULL(fna.AccountDescription,'') AS AccountDescription,
		   ISNULL(fna.DetailDescription,'') AS DetailDescription,
		   ISNULL(fna.AccountType,'') AS AccountType,
		   ISNULL(gla.BudgetType,'') AS BudgetType,

		CASE WHEN s.SubLedgerCode = 'AP' AND JD.Source != 'Void Payment' AND fna.AccountType > 3 THEN ISNULL(apdetl.amount,0)
	   ELSE
            ISNULL(jd.Amount,0)
		END AS Amount,
		--CASE WHEN s.SubLedgerCode != 'AP' THEN ISNULL(jd.Amount, 0)
		--   ELSE
        --    apdetl.amount
		--END AS amount,
		JD.FiscalEndYear,
		JH.JournalType,
		JD.Description AS Description,
		ISNULL(JD.Source, '') AS Source,
		S.SubLedgerName AS SubLedgerName,
		JH.JournalNumber,
    JD.ChangedDate AS JDChangedDate,
    JH.ChangedDate AS JHChangedDate,
		JH.EnteredDate,
		apdetl.invoiceNumber,
		apdetl.itemDescription,
		apdetl.vendorNumber,
		apdetl.vendorName,
		s.SubLedgerCode

	FROM LogosDB.dbo.JournalDetail JD
		JOIN LogosDB.dbo.JournalHeader JH ON JD.JournalID = JH.JournalID
		JOIN LogosDB.dbo.SubLedger S ON JH.SubLedger = S.SubLedgerID
	JOIN LogosDB.dbo.fn_GLAccountWithDescription((SELECT OrganizationDelimiter FROM cte),
		                                     (SELECT OrgSetandAccountDelimiter FROM cte),
											 (SELECT AccountDelimiter FROM cte)) fnG
			ON JD.GLAccountID = fnG.GLAccountID
		JOIN LogosDB.dbo.GLAccount GLA ON JD.GLAccountID = GLA.GLAccountID
		JOIN LogosDB.dbo.Fn_Accounts((SELECT AccountDelimiter FROM cte)) FnA ON GLA.AccountID = FnA.AccountID
		LEFT JOIN LogosDB.dbo.OrganizationSet os ON os.OrgSetID = GLA.OrgSetID
		LEFT JOIN LogosDB.dbo.JournalDetailAPSubLedger sap ON sap.JournalDetailID = jd.JournalDetailID
		            AND ISNULL(fna.AccountType,'') > '2' AND JD.Source != 'Void Payment'
	    LEFT JOIN (
		        SELECT apgl.GLAccountID,
			       api.InvoiceID,
					   CASE WHEN apgl.DistributionPercentage IS NULL THEN ISNULL(apgl.DistributionAmount,0)
		                    ELSE (apii.ItemQuantity * apii.ItemPrice) * apgl.DistributionPercentage
                      END AS amount,
				       --sap.JournalDetailID,
				       ISNULL(api.InvoiceNumber, '') AS invoiceNumber,
		               ISNULL(apii.ItemDescription, '') AS itemDescription,
		               ISNULL(v.VendorNumber, 0) AS vendorNumber,
		               ISNULL(vc.ContactName,'') AS vendorName,
		               ISNULL(apgl.DistributionAmount,0) AS distrubutionAmount,
		               ISNULL(apgl.DistributionPercentage,0) AS distributionPct,
	               ISNULL(api.InvoiceAmount, 0) AS invoiceAmount,
	               ISNULL(apii.ItemQuantity, 0) AS itemQuantity,
		               ISNULL(apii.ItemPrice,0) AS itemPrice
 		        FROM LogosDB.dbo.AccountsPayableInvoice api
		        JOIN LogosDB.dbo.AccountsPayableInvoiceItem apii ON apii.InvoiceID = api.InvoiceID
		        JOIN LogosDB.dbo.AccountsPayableItemGLDistribution apgl ON apgl.InvoiceID = api.InvoiceID
		                                AND apgl.InvoiceItemID = apii.InvoiceItemID
	        JOIN LogosDB.dbo.Vendor v ON v.VendorID = api.VendorID
		        JOIN LogosDB.dbo.VendorContact vc ON vc.VendorContactID = api.VendorContactID
		) AS apdetl ON apdetl.GLAccountID = jd.GLAccountID AND apdetl.InvoiceID = sap.InvoiceID

		WHERE jh.ProcessStatus = 2
