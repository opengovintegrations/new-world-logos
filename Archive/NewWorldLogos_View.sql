CREATE VIEW logos_opengov_transactions
as


WITH cte AS
( SELECT OrganizationDelimiter,
         OrgSetandAccountDelimiter,
	     AccountDelimiter
 FROM LogosDB.dbo.LogosSystemSettings
)


SELECT  jd.JournalDetailID AS transactionID,
    JD.GLDate,
		GLA.GLAccountCodeScrunched,
		fnG.GLAccountDelimiter,
		ISNULL(GLA.Org1Code, '') AS Org1Code,
		ISNULL(os.Org1Description,'') AS Org1Description,
		ISNULL(GLA.Org2Code,'') AS Org2Code,
		ISNULL(os.Org2Description,'') AS Org2Description,
		ISNULL(GLA.Org3Code,'') AS Org3Code,
		ISNULL(os.Org3Description,'') AS Org3Description,
		ISNULL(GLA.Org4Code,'') AS Org4Code,
		ISNULL(os.Org4Description,'') AS Org4Description,
		ISNULL(GLA.Org5Code,'') AS Org5Code,
		ISNULL(os.Org5Description,'') AS Org5Description,
		ISNULL(GLA.Org6Code,'') AS Org6Code,
		ISNULL(os.Org6Description,'') AS Org6Description,
		ISNULL(FnA.AccountCode,'') AS AccountCode,
		ISNULL(FnA.DetailCode,'') AS DetailCode,
		ISNULL(FnA.SubDetailCode,'') AS SubDetailCode,
		ISNULL(fna.AccountDescription,'') AS AccountDescription,
		ISNULL(fna.DetailDescription,'') AS DetailDescription,
		ISNULL(fna.AccountType,'') AS AccountType,
		ISNULL(gla.BudgetType,'') AS BudgetType,
		--JD.ProjectRevenueFlag,
		JD.Amount AS Amount,
		JD.FiscalEndYear,
		JH.JournalType,
		--JD.Prior
    YearActivityFlag,
		JD.Description AS Description,
		ISNULL(JD.Source, '') AS Source,
		S.SubLedgerName AS SubLedgerName,
		JH.JournalNumber,
		JH.EnteredDate
		--EnteredUser = SU.UserName
	FROM LogosDB.dbo.JournalDetail JD
		JOIN LogosDB.dbo.JournalHeader JH ON JD.JournalID = JH.JournalID
		JOIN LogosDB.dbo.SubLedger S ON JH.SubLedger = S.SubLedgerID
		JOIN LogosDB.dbo.fn_GLAccountWithDescription((SELECT OrganizationDelimiter FROM cte),
		                                     (SELECT OrgSetandAccountDelimiter FROM cte),
											 (SELECT AccountDelimiter FROM cte)) fnG
			ON JD.GLAccountID = fnG.GLAccountID
		JOIN LogosDB.dbo.GLAccount GLA ON JD.GLAccountID = GLA.GLAccountID
		JOIN LogosDB.dbo.Fn_Accounts((SELECT AccountDelimiter FROM cte)) FnA ON GLA.AccountID = FnA.AccountID
		LEFT JOIN LogosDB.dbo.OrganizationSet os ON os.OrgSetID = GLA.OrgSetID
where jh.ProcessStatus = 2
