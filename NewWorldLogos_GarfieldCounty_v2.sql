ALTER VIEW [dbo].[vw_logos_opengov_transactions_with_ap_detail_v2]

AS

WITH cte AS

( SELECT OrganizationDelimiter,
         OrgSetandAccountDelimiter,
	     AccountDelimiter
 FROM logoslive.dbo.LogosSystemSettings
),

INVOICEJOURNAL AS

(SELECT
		JD.JournalDetailID as TransactionID,
        JD.GLDate,
		GLA.GLAccountCodeScrunched,
		fnG.GLAccountDelimiter,
		GLA.AccountID,
		GLA.GLAccountID,
		ISNULL(GLA.Org1Code, '') AS Org1Code,
		ISNULL(os.Org1Description,'') AS Org1Description,
		ISNULL(GLA.Org2Code,'') AS Org2Code,
		ISNULL(os.Org2Description,'') AS Org2Description,
		ISNULL(GLA.Org3Code,'') AS Org3Code,
		ISNULL(os.Org3Description,'') AS Org3Description,
		ISNULL(GLA.Org4Code,'') AS Org4Code,
		ISNULL(os.Org4Description,'') AS Org4Description,
		ISNULL(GLA.Org5Code,'') AS Org5Code,
		ISNULL(os.Org5Description,'') AS Org5Description,
		ISNULL(GLA.Org6Code,'') AS Org6Code,
		ISNULL(os.Org6Description,'') AS Org6Description,
		ISNULL(FnA.AccountCode,'') AS AccountCode,
		ISNULL(FnA.DetailCode,'') AS DetailCode,
		ISNULL(FnA.SubDetailCode,'') AS SubDetailCode,
		ISNULL(fna.AccountDescription,'') AS AccountDescription,
		ISNULL(fna.DetailDescription,'') AS DetailDescription,
		ISNULL(fna.AccountType,'') AS AccountType,
		ISNULL(gla.BudgetType,'') AS BudgetType,

		JD.FiscalEndYear,
		JH.JournalType,
		JD.Description AS Description,
		ISNULL(JD.Source, '') AS Source,
		S.SubLedgerName AS SubLedgerName,
		JH.JournalNumber,
		JH.EnteredDate,
		JH.ChangedDate AS JHChangedDate,
		JD.ChangedDate AS JDChangedDate,

		s.SubLedgerCode,

		ISNULL(jd.Amount,0) Amount,

		(SELECT TOP 1 InvoiceID 
			FROM logoslive.dbo.JournalDetailAPSubLedger sap
			WHERE sap.JournalDetailID = jd.JournalDetailID) AS InvoiceID

	FROM logoslive.dbo.JournalDetail JD
		JOIN logoslive.dbo.JournalHeader JH ON JD.JournalID = JH.JournalID
		JOIN logoslive.dbo.SubLedger S ON JH.SubLedger = S.SubLedgerID
		JOIN logoslive.dbo.fn_GLAccountWithDescription(
											(SELECT OrganizationDelimiter FROM cte),
		                                     (SELECT OrgSetandAccountDelimiter FROM cte),
											 (SELECT AccountDelimiter FROM cte)) fnG
			ON JD.GLAccountID = fnG.GLAccountID
		JOIN logoslive.dbo.GLAccount GLA ON JD.GLAccountID = GLA.GLAccountID
		JOIN logoslive.dbo.Fn_Accounts((SELECT AccountDelimiter FROM cte)) FnA ON GLA.AccountID = FnA.AccountID
		LEFT JOIN logoslive.dbo.OrganizationSet os ON os.OrgSetID = GLA.OrgSetID

WHERE jh.ProcessStatus = 2
	--AND ISNULL(fna.AccountType,'') > '2' 
	--AND JD.Source != 'Void Payment'
	)

-----------------------------------------

,JOURNALNOINVOICE AS
(SELECT
		JD.JournalDetailID as TransactionID,
        JD.GLDate,
		GLA.GLAccountCodeScrunched,
		fnG.GLAccountDelimiter,
		GLA.AccountID,
		GLA.GLAccountID,
		ISNULL(GLA.Org1Code, '') AS Org1Code,
		ISNULL(os.Org1Description,'') AS Org1Description,
		ISNULL(GLA.Org2Code,'') AS Org2Code,
		ISNULL(os.Org2Description,'') AS Org2Description,
		ISNULL(GLA.Org3Code,'') AS Org3Code,
		ISNULL(os.Org3Description,'') AS Org3Description,
		ISNULL(GLA.Org4Code,'') AS Org4Code,
		ISNULL(os.Org4Description,'') AS Org4Description,
		ISNULL(GLA.Org5Code,'') AS Org5Code,
		ISNULL(os.Org5Description,'') AS Org5Description,
		ISNULL(GLA.Org6Code,'') AS Org6Code,
		ISNULL(os.Org6Description,'') AS Org6Description,
		ISNULL(FnA.AccountCode,'') AS AccountCode,
		ISNULL(FnA.DetailCode,'') AS DetailCode,
		ISNULL(FnA.SubDetailCode,'') AS SubDetailCode,
		ISNULL(fna.AccountDescription,'') AS AccountDescription,
		ISNULL(fna.DetailDescription,'') AS DetailDescription,
		ISNULL(fna.AccountType,'') AS AccountType,
		ISNULL(gla.BudgetType,'') AS BudgetType,

		JD.FiscalEndYear,
		JH.JournalType,
		JD.Description AS Description,
		ISNULL(JD.Source, '') AS Source,
		S.SubLedgerName AS SubLedgerName,
		JH.JournalNumber,
		JH.EnteredDate,
		JH.ChangedDate AS JHChangedDate,
		JD.ChangedDate AS JDChangedDate,

		s.SubLedgerCode,

		ISNULL(jd.Amount,0) Amount

	FROM logoslive.dbo.JournalDetail JD
		JOIN logoslive.dbo.JournalHeader JH ON JD.JournalID = JH.JournalID
		JOIN logoslive.dbo.SubLedger S ON JH.SubLedger = S.SubLedgerID
		JOIN logoslive.dbo.fn_GLAccountWithDescription(
											(SELECT OrganizationDelimiter FROM cte),
		                                     (SELECT OrgSetandAccountDelimiter FROM cte),
											 (SELECT AccountDelimiter FROM cte)) fnG
			ON JD.GLAccountID = fnG.GLAccountID
		JOIN logoslive.dbo.GLAccount GLA ON JD.GLAccountID = GLA.GLAccountID
		JOIN logoslive.dbo.Fn_Accounts((SELECT AccountDelimiter FROM cte)) FnA ON GLA.AccountID = FnA.AccountID
		LEFT JOIN logoslive.dbo.OrganizationSet os ON os.OrgSetID = GLA.OrgSetID

WHERE jh.ProcessStatus = 2
	--AND ISNULL(fna.AccountType,'') > '2' 
	--AND JD.Source != 'Void Payment'
	AND JD.JournalDetailID not in (SELECT DISTINCT
									JournalDetailID
									FROM logoslive.dbo.JournalDetailAPSubLedger sap))

,ALLTXNS AS
(SELECT INVOICEJOURNAL.*,
	ISNULL(api.InvoiceNumber, '') AS InvoiceNumber,
	api.Description AS InvoiceDescription,
	ISNULL(v.VendorNumber, 0) AS vendorNumber,
	ISNULL(vc.ContactName,'') AS vendorName
	FROM INVOICEJOURNAL
	LEFT JOIN logoslive.dbo.AccountsPayableInvoice api ON
		INVOICEJOURNAL.InvoiceID = api.InvoiceID
	LEFT JOIN logoslive.dbo.Vendor v ON 
		v.VendorID = api.VendorID
	JOIN logoslive.dbo.VendorContact vc ON 
		vc.VendorContactID = api.VendorContactID
UNION
SELECT JOURNALNOINVOICE.*,
'' AS InvoiceID,
'' AS invoiceNumber,
'' AS InvoiceDescription,
'' AS vendorNumber,
'' AS vendorName
FROM JOURNALNOINVOICE)

SELECT * FROM ALLTXNS