CREATE VIEW [dbo].[Transactions] AS

SELECT

	[GL Account],

	LEFT([Fund], 3) as [Fund],

	LEFT([Division], 2) as [Division],

	LEFT([Program], 3) as [Program],

	CONCAT([AccountCode], '.', [DetailCode]"
	[Invoice Date],

	[Invoice Number],

	[Invoice Description],

	[Vendor Number],

	[Vendor],

	[Invoice Amount],

	[Changed Date]

	FROM

	(SELECT

		gl.[GLAccount Delimited] AS [GL Accoun"
		org.[Org1CodeDesc] AS [Fund],

		org.[Org2CodeDesc] AS [Division],

		org.[Org3CodeDesc] AS [Program],

		gl.[AccountCode],

		gl.[DetailCode],

		CONVERT(date, CONVERT(varchar, ap.[Inv"
		inv.[Invoice Number], 

		inv.[Invoice Description], 

		vnd.[VendorID] AS [Vendor Number],

		vnd.[VendorName] AS [Vendor], 

		SUM(ap.[InvoiceAmount]) AS [Invoice Am"
		ap.[ChangedDate] AS [Changed Date]

		FROM  [FM].[Fact_APInvoiceDetail] AS a"
		JOIN [Common].[Dim_OrganizationSet] AS"
		ON (ap.OrgSetKey = org.OrgSetKey)

		JOIN [Common].[Dim_GLAccount] AS gl

		ON (ap.GLAccountKey = gl.GLAccountKey)"
		JOIN [FM].[Dim_Vendor] AS vnd

		ON (ap.VendorKey = vnd.VendorKey)

		JOIN [FM].[Dim_APInvoice] AS inv

		ON (ap.InvoiceKey = inv.InvoiceKey)

		WHERE [Transaction Type] = 'Invoice'

		AND [AccountType] = 'Expenses'

		AND [Invoice Batch Status] = 'Posted'

		GROUP BY

		gl.[GLAccount Delimited],

		org.[Org1CodeDesc],

		org.[Org2CodeDesc],

		org.[Org3CodeDesc], 

		gl.[AccountCode],

		gl.[DetailCode],

		ap.[InvoiceGLDateKey], 

		inv.[Invoice Number], 

		inv.[Invoice Description], 

		vnd.[VendorName],

		vnd.[VendorID],

		ap.[ChangedDate])t

	WHERE [Invoice Date] >= '2016-07-01'

